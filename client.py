import time
import sys
import struct
import socket
import threading
from enum import Enum
from typing import Callable, List, Optional


class ServerUnavailable(Exception):
    pass


class MyBaseClient:

    _addr: str
    _port: int
    _socket: Optional[socket.socket]
    _thread: threading.Thread
    _lock: threading.Lock
    _working: bool = False
    _target_working: bool = False

    @property
    def addr(self) -> str:
        return self._addr

    @property
    def port(self) -> int:
        return self._port

    @property
    def is_working(self) -> bool:
        return self._working

    def __init__(self, sock: Optional[socket.socket], addr: str, port: int):
        super().__init__()
        if sock:
            sock.settimeout(0.1)

        self._socket = sock
        self._lock = threading.Lock()
        self._addr = addr
        self._port = port

    def on_recive_message(self, data_buf: bytes):
        pass

    def _worker(self):
        self._working = True
        while self._target_working:
            size_buf = self._recv_block(4)
            if size_buf is None:
                continue
            size, = struct.unpack('<I', size_buf)
            data_buf = self._recv_block(size)
            if data_buf is None:
                continue
            self.on_recive_message(data_buf)
        self._working = False

    def start(self):
        if self._target_working: return
        self._target_working = True

        self._thread = threading.Thread(target=self._worker)
        self._thread.start()

    def stop_signal(self):
        self._target_working = False

    def join(self):
        self._thread.join()

    def close(self):
        if not self._socket:
            return
        with self._lock:
            try:
                self._socket.shutdown(socket.SHUT_RDWR)
            except OSError:
                pass
            self._socket.close()
            self._socket = None

    def send(self, data: bytes):
        if not self._socket:
            raise ValueError('socket is None')
        data_to_send = struct.pack('<I', len(data)) + data

        with self._lock:
            sended = 0
            while self._target_working and sended < len(data_to_send):
                sended += self._socket.send(data_to_send[sended:])

    def _recv_block(self, count: int) -> Optional[bytes]:
        if not self._socket:
            raise ValueError('socket is None')

        if count <= 0: return b''

        buf = []
        buf_len = 0
        while buf_len < count:
            if not self._target_working:
                return None
            try:
                buf2 = self._socket.recv(count - buf_len)
            except socket.timeout:
                continue
            if not buf2: raise ConnectionError()  # error or just end of connection
            buf.append(buf2)
            buf_len += len(buf2)

        res = b''.join(buf)
        assert len(res) == buf_len
        return res


class MyClient(MyBaseClient):
    """Основа клиента"""

    def __init__(self, addr: str, port: int):
        super().__init__(None, addr, port)

    def connect(self):
        if self._socket: return

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self._socket.connect((self._addr, self._port))
        except (ConnectionError, socket.gaierror) as e:
            raise ServerUnavailable from e
        self._socket.settimeout(0.1)

    def _worker(self):
        try:
            super()._worker()
        finally:
            self._working = False
            self.close()



class MasterGameClient(MyClient):

    the_server_is_full: bool = False
    game_port: Optional[int] = None

    def on_recive_message(self, data_buf: bytes):
        if data_buf == b'The server is full':
            self.the_server_is_full = True
            self.stop_signal()
            return
        if len(data_buf) != 4:
            self.stop_signal()
            return
        self.game_port, = struct.unpack('<I', data_buf)

    def _worker(self):
        try:
            return super()._worker()
        except ConnectionError:
            pass



class GameStage(Enum):
    START = 0
    PLAING = 1
    END = 2

class GameClient(MyClient):

    _last_message: Optional[str] = ''
    _game_stage: GameStage = GameStage.START

    def on_recive_message(self, data_buf: bytes):
        with self._lock:
            self._last_message = data_buf.decode()
            print(self._last_message)

    def play_game(self):
        """
        В одном случе нам сообщение сервер присылает, в другом мы сами его должны генерировать.
        Итогда на одно сообщение клиента, сервер отправляет одно в ответ, а иногда два.
        Иногда на сообщение сервера нужно что-то отвечать, а иногда не нужно.
        Что за бред...
        """

        def input_round():
            use_server_message = True
            while self._working:
                # Sometimes we need to generate print message, sometimes - no
                if not use_server_message:
                    if self._game_stage == GameStage.START:
                        print('Enter the range:')
                    elif self._game_stage == GameStage.PLAING:
                        print('Please, enter a single number:')
                use_server_message = False

                cmd = input('> ')  # we don't have to worry about incoming messages, so we can use blocking IO
                if self._game_stage == GameStage.START:
                    cmd_range = cmd.strip().split()
                    if len(cmd_range) != 2: continue
                    try:
                        A, B = int(cmd_range[0]), int(cmd_range[1])
                    except ValueError:
                        continue
                    self.send(struct.pack('<ii', A, B))
                    self._game_stage = GameStage.PLAING
                    return
                elif self._game_stage == GameStage.PLAING:
                    cmd_range = cmd.strip().split()
                    if len(cmd_range) != 1: continue
                    try:
                        A = int(cmd_range[0])
                    except ValueError:
                        continue
                    self.send(struct.pack('<i', A))
                    return

        while self._working:
            # wait for new message from server
            while self._working:
                with self._lock:
                    if self._last_message is not None and (self._last_message.endswith('attempts') or self._last_message.endswith('the range:')):
                        self._last_message = None
                        break
                time.sleep(0.02)
            if not self._working:
                return
            input_round()
            

    def _worker(self):
        try:
            return super()._worker()
        except ConnectionError:
            self.close()


def main():
    if len(sys.argv) != 3:
        print(f'Usage example: python {sys.argv[0]} <address> <port>')
        exit(1)

    try:
        port = int(sys.argv[2])
    except ValueError as e:
        print(f'Cannot convert port to int')
        exit(1)

    client = MasterGameClient(sys.argv[1], port)

    try:
        client.connect()
    except ServerUnavailable:
        print('Server is unavailable')
        return

    client.start()
    client.join()

    if client.the_server_is_full:
        print('The server is full')
        return

    if client.game_port is None:
        print('Server do not send game server port')
        return

    game_client = GameClient(sys.argv[1], client.game_port)

    try:
        game_client.connect()
    except ConnectionError:
        print('Game server is unavailable')
        return

    game_client.start()
    try:
        game_client.play_game()
        game_client.join()
    finally:
        game_client.stop_signal()
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
