import sys
import time
import threading
import socket
import struct
import random
from enum import Enum
from typing import Callable, List, Optional


class MyServerException(Exception):
    pass

class FailedToBindServerException(MyServerException):
    pass


class MyBaseClient:

    _addr: str
    _port: int
    _socket: Optional[socket.socket]
    _thread: threading.Thread
    _lock: threading.Lock
    _working: bool = False
    _target_working: bool = False

    @property
    def addr(self) -> str:
        return self._addr

    @property
    def port(self) -> int:
        return self._port

    @property
    def is_working(self) -> bool:
        return self._working

    def __init__(self, sock: Optional[socket.socket], addr: str, port: int):
        super().__init__()
        if sock:
            sock.settimeout(0.1)

        self._socket = sock
        self._lock = threading.Lock()
        self._addr = addr
        self._port = port

    def on_recive_message(self, data_buf: bytes):
        pass

    def _worker(self):
        self._working = True
        while self._target_working:
            size_buf = self._recv_block(4)
            if size_buf is None:
                continue
            size, = struct.unpack('<I', size_buf)
            data_buf = self._recv_block(size)
            if data_buf is None:
                continue
            self.on_recive_message(data_buf)
        self._working = False

    def start(self):
        if self._target_working: return
        self._target_working = True

        self._thread = threading.Thread(target=self._worker)
        self._thread.start()

    def stop_signal(self):
        self._target_working = False

    def join(self):
        self._thread.join()

    def close(self):
        if not self._socket:
            return
        with self._lock:
            try:
                self._socket.shutdown(socket.SHUT_RDWR)
            except OSError:
                pass
            self._socket.close()
            self._socket = None

    def send(self, data: bytes):
        if not self._socket:
            raise ValueError('socket is None')
        data_to_send = struct.pack('<I', len(data)) + data

        with self._lock:
            sended = 0
            while self._target_working and sended < len(data_to_send):
                sended += self._socket.send(data_to_send[sended:])

    def _recv_block(self, count: int) -> Optional[bytes]:
        if not self._socket:
            raise ValueError('socket is None')

        if count <= 0: return b''

        buf = []
        buf_len = 0
        while buf_len < count:
            if not self._target_working:
                return None
            try:
                buf2 = self._socket.recv(count - buf_len)
            except socket.timeout:
                continue
            if not buf2: raise ConnectionError()  # error or just end of connection
            buf.append(buf2)
            buf_len += len(buf2)

        res = b''.join(buf)
        assert len(res) == buf_len
        return res


class MyServerClient(MyBaseClient):
    """Представление клиента на стороне сервера"""

    _recv_callback: Optional[Callable[['MyServerClient', bytes], None]] = None

    def __init__(self, sock: socket.socket, addr: str, port: int, recv_callback: Callable[['MyServerClient', bytes], None]):
        super().__init__(sock, addr, port)
        self._recv_callback = recv_callback

    def on_recive_message(self, data_buf: bytes):
        if self._recv_callback:
            self._recv_callback(self, data_buf)

    def _worker(self):
        try:
            super()._worker()
        except ConnectionError:
            pass
        finally:
            self._target_working = False
            self._working = False
            self.close()

    def send(self, data: bytes):
        if not self._target_working: return
        try:
            super().send(data)
        except ConnectionError:
            self._target_working = False
            self._working = False
            self.close()


class MyServer:
    """Основа сервера"""

    _addr: str
    _port: int
    _socket: socket.socket
    _thread: threading.Thread
    _clients: List[MyServerClient] = []
    _target_working: bool = False
    _working: bool = False

    @property
    def port(self) -> int:
        return self._port

    @property
    def is_working(self) -> bool:
        return self._working

    def __init__(self, addr: str, port: int):
        super().__init__()
        self._addr = addr
        self._port = port
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def start(self):
        if self._target_working: return

        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.settimeout(0.1)
        try:
            self._socket.bind((self._addr, self._port))
        except (OverflowError, OSError) as e:
            raise FailedToBindServerException from e
        self._socket.listen(5)

        self._target_working = True
        self._thread = threading.Thread(target=self._worker)
        self._thread.start()

    def stop_signal(self):
        self._target_working = False

        if not self._target_working:
            for cli in self._clients:
                cli.stop_signal()

    def join(self):
        self._thread.join()

        for cli in self._clients:
            cli.join()

    def on_start_server(self):
        pass

    def on_stop_server(self):
        pass

    def on_new_connection(self, client: MyServerClient):
        pass

    def on_recive_message(self, client: MyServerClient, data_buf: bytes):
        pass

    def on_accept_timeout(self):
        pass

    def _check_clients(self):
        dead_clients = [cli for cli in self._clients if not cli.is_working]
        self._clients = [cli for cli in self._clients if cli.is_working]

        if len(dead_clients) > 0:
            # logger.debug('{} клиентов отключились от сервера', len(dead_clients))

            for cli in dead_clients:
                cli.join()

    def _worker(self):
        # logger.info("Server has started on {}, {}", self._addr, self._port)
        self._working = True
        self.on_start_server()

        while self._target_working:
            try:
                conn, addr = self._socket.accept()
            except socket.timeout:
                self.on_accept_timeout()
                continue

            client = MyServerClient(conn, addr[0], addr[1], self.on_recive_message)
            client.start()
            self._clients.append(client)
            self.on_new_connection(client)
            self._check_clients()

        self.on_stop_server()
        self._working = False

    def send_broadcast(self, data: bytes):
        for cli in self._clients:
            cli.send(data)




class GameStage(Enum):
    START = 0
    PLAING = 1
    END = 2

class GameServer(MyServer):

    _start_time: int = 0
    _my_client: Optional[MyServerClient] = None
    _game_stage: GameStage = GameStage.START
    _game_answer: int = 0
    _number_of_guesses_left: int = 5

    def __init__(self):
        super().__init__('127.0.0.1', 0)

    def start(self):
        self._start_time = time.monotonic_ns()
        while True:
            try:
                self._port = random.randint(1024, 65535)
                return super().start()
            except FailedToBindServerException:
                pass

    def on_accept_timeout(self):
        if time.monotonic_ns() - self._start_time > 5e9 and not self._my_client:
            self.stop_signal()

        if self._my_client and not self._my_client.is_working:
            self.stop_signal()

    def on_new_connection(self, client: MyServerClient):
        if self._my_client:
            client.stop_signal()
            return
        self._my_client = client
        self._my_client.send(b'Welcome to the number guessing game!\nEnter the range:')

    def on_recive_message(self, client: MyServerClient, data_buf: bytes):
        if self._my_client != client:
            return

        if (self._game_stage == GameStage.START) and (len(data_buf) == 8):
            A, B = struct.unpack('<ii', data_buf)
            self._game_answer = random.randint(min(A, B), max(A, B))
            self._game_stage = GameStage.PLAING
            client.send(f'You have {self._number_of_guesses_left} attempts'.encode())
        elif (self._game_stage == GameStage.PLAING) and (len(data_buf) == 4):
            A, = struct.unpack('<i', data_buf)
            self._number_of_guesses_left -= 1
            if A == self._game_answer:
                client.send(b'You win!')
                self._game_stage = GameStage.END
                self.stop_signal()
            elif self._number_of_guesses_left == 0:
                client.send(b'You lose ')
                self._game_stage = GameStage.END
                self.stop_signal()
            else:
                if A < self._game_answer:
                    client.send(b'Greater')
                else:
                    client.send(b'Less')
                client.send(f'You have {self._number_of_guesses_left} attempts'.encode())



class MasterGameServer(MyServer):

    MAX_CLIENTS = 2
    _game_servers: List[GameServer] = []

    def __init__(self, port: int):
        super().__init__('127.0.0.1', port)

    def stop_signal(self):
        super().stop_signal()

        for cli in self._game_servers:
            cli.stop_signal()

    def join(self):
        super().join()

        for cli in self._game_servers:
            cli.join()

    def _check_game_servers(self):
        dead_servers = [cli for cli in self._game_servers if not cli.is_working]
        self._game_servers = [cli for cli in self._game_servers if cli.is_working]

        for cli in dead_servers:
            cli.join()

    def on_new_connection(self, client: MyServerClient):
        self._check_game_servers()

        if len(self._game_servers) + 1 > self.MAX_CLIENTS:
            print('The server is full')
            client.send(b'The server is full')
            client.stop_signal()
            return

        print('Client connected')
        game_server = GameServer()
        game_server.start()

        self._game_servers.append(game_server)

        client.send(struct.pack('<I', game_server.port))
        client.stop_signal()

    def on_recive_message(self, client: MyServerClient, data_buf: bytes):
        pass



def main():
    if len(sys.argv) != 2:
        print(f'Usage example: python {sys.argv[0]} <port>')
        exit(1)

    try:
        port = int(sys.argv[1])
    except ValueError as e:
        print(f'Cannot convert port to int')
        exit(1)

    server = MasterGameServer(port)

    try:
        server.start()
    except FailedToBindServerException:
        print('Error while binding to the specified port')
        exit(1)

    print(f'Starting the server on 127.0.0.1:{port}')
    print(f'Waiting for a connection')

    try:
        while True:
            time.sleep(1)
    finally:
        server.stop_signal()
        server.join()
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass